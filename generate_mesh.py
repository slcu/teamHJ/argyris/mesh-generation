import vtk
import tifffile as tiff
from scipy.ndimage.morphology import binary_fill_holes
from skimage.exposure import equalize_hist, equalize_adapthist
import phenotastic.plot as pl
import os
import numpy as np
import phenotastic.Meristem_Phenotyper_3D as ap
import copy
from skimage import measure
from vtk.util import numpy_support as nps
from scipy.ndimage.filters import gaussian_filter, median_filter
from skimage.segmentation import morphological_chan_vese
import vtkInterface as vi
from vtkInterface.common import AxisRotation
import phenotastic.domain_processing as boa
import phenotastic.mesh_processing as mp
import phenotastic.file_processing as fp
from phenotastic.misc import mkdir, listdir, autocrop
import gc
import mahotas as mh
from skimage.exposure import rescale_intensity
from scipy.ndimage.morphology import generate_binary_structure
import matplotlib.pyplot as plt
import os.path


class IndexTracker(object):
    def __init__(self, ax, X):
        self.ax = ax
        ax.set_title('use scroll wheel to navigate images')

        self.X = X
        self.slices, rows, cols = X.shape
        self.ind = self.slices//2

        self.im = ax.imshow(self.X[self.ind, :, :])
        self.update()

    def onscroll(self, event):
        if event.button == 'up':
            self.ind = (self.ind + 1) % self.slices
        else:
            self.ind = (self.ind - 1) % self.slices
        self.update()

    def update(self):
        self.im.set_data(self.X[self.ind, :, :])
        self.ax.set_ylabel('slice %s' % self.ind)
        self.im.axes.figure.canvas.draw()

def mkMeshFromFile(fin, npoints):
    (root, ext) = os.path.splitext(fin)
    
    ctrFn = root + "_auto.ctr"
    meshFn = root + "_auto.ply"
    
    A = readData(fin)
    smoothData(A)
    getContour(A)
    print "got contour"

    ctr = A.contour.astype(np.uint8)
    fp.tiffsave(ctrFn, ctr)
    
    mkMesh(A)
    mkSurface(A)
    genFixMesh(A)
    remesh(A, npoints)
    smooth(A, iterations=100, relaxation_factor=.01)
    genFixMesh(A)

    A.mesh = mp.remove_tongues(A.mesh, radius=50, threshold=2, threshold2=.8)
    A.mesh = mp.remove_bridges(A.mesh)

    A.mesh.Write(meshFn, 'ply', binary=False)

def mkMeshFromFileToMesh(fin, npoints):
    A = readData(fin)
    smoothData(A)
    getContour(A)
    print "got contour"
    mkMesh(A)
    mkSurface(A)
    genFixMesh(A)
    remesh(A, npoints)
    smooth(A, iterations=100, relaxation_factor=.01)
    genFixMesh(A)

    return A

def plot3d(data):
    fig, ax = plt.subplots(1, 1)
    tracker = IndexTracker(ax, data)
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show()


def readData(fin):
    f = fp.tiffload(fin)
    meta = f.metadata

    resolution = fp.get_resolution(f)

    data = f.data.astype(np.float)
    data = autocrop(data, 5000, fct=np.max)

    data = data[:, 0, :, :]

    A = ap.AutoPhenotype()
    A.data = data
    A.data = A.data.astype(np.uint16)

    A.data = A.data.astype(np.float)
    A.data = A.data / np.max(A.data)

    A.res = resolution

    return A

def smoothData(A, niter=1):
    resolution = A.res
    for ii in xrange(niter):
        A.data = median_filter(A.data, footprint=generate_binary_structure(3,2))

    for ii in xrange(3):
        A.data = gaussian_filter(
            A.data, sigma=[2. * .25 / resolution[0],
                           2. * .25 / resolution[1],
                           2. * .25 / resolution[2]])

def getContour(A):
    from skimage.morphology import binary_opening, binary_closing, binary_erosion
    from scipy.ndimage import generate_binary_structure
    
    factor = .5
    contour = morphological_chan_vese(A.data, iterations=10,
                                      init_level_set=A.data > factor * np.mean(A.data),
                                      smoothing=1, lambda1=1, lambda2=1)
    contour = binary_erosion(contour, generate_binary_structure(3,2))
    contour = mp.fill_contour(contour, fill_xy=False)
    contour = binary_erosion(contour, generate_binary_structure(3,2))

    A.contour = contour

def loadContour(fin):
    f = fp.tvfile(fin)

    return f.asarray()

def safeHead(xs, d):
    if xs.size == 0: return d 
    else: return xs[0]

def fillContourLeft(A):
    from skimage.morphology import binary_opening, binary_closing, binary_erosion
    from scipy.ndimage import generate_binary_structure
    
    ctr = A.contour.copy()
    x, y, z = np.shape(ctr)

    for i in xrange(z):
        for k in xrange(x):
            nz = safeHead(np.nonzero(ctr[k, :, i])[0], 0)
            ctr[k, :nz, i] = 1

    ctr = binary_erosion(ctr, generate_binary_structure(3,2))
    ctr = mp.fill_contour(ctr, fill_xy=False)
    ctr = binary_erosion(ctr, generate_binary_structure(3,2))

    A.contour = ctr.copy()
    
def mkMesh(A):
    verts, faces, normals, values = measure.marching_cubes_lewiner(
        A.contour, 0, spacing=list(A.res / np.min(A.res)), step_size=1,
        allow_degenerate=False)
    
    faces = np.hstack(np.c_[np.full(faces.shape[0], 3), faces])
    A.mesh = vi.PolyData(verts, faces)


def mkSurface(A):
    c = 2

    A.mesh = A.mesh.ClipPlane([A.mesh.points[:,0].min() + c, 0, 0], [1, 0, 0], inplace=False)
    A.mesh = A.mesh.ClipPlane([0, A.mesh.points[:, 1].min() + c, 0], [0, 1, 0], inplace=False)
    A.mesh = A.mesh.ClipPlane([0, 0, A.mesh.points[:, 1].min() + c], [0, 0, 1], inplace=False)
    
    A.mesh = mp.correct_bad_mesh(A.mesh)
    A.mesh = mp.ECFT(A.mesh, 100)
    A.mesh = A.mesh.GenerateNormals(inplace=False)

    return


def remesh(A, npoints, niter=2):
    A.mesh = A.mesh.Decimate(
        0.95, volume_preservation=True, normals=True, inplace=False)
    A.mesh = mp.ECFT(A.mesh, 0)

    A.mesh = mp.correct_bad_mesh(A.mesh)
    A.mesh = mp.ECFT(A.mesh, 100)

    for i in range(niter):
        A.mesh = mp.remesh(A.mesh, npoints)
        genFixMesh(A)

    A.mesh = A.mesh.GenerateNormals(inplace=False)

    return

def genFixMesh(A):
    A.mesh = mp.correct_bad_mesh(A.mesh)
    A.mesh = mp.ECFT(A.mesh, 100)

    A.mesh = A.mesh.GenerateNormals(inplace=False)
    
    if np.sum(nps.vtk_to_numpy(A.mesh.GetPointData().GetNormals()), axis=0)[0] < 0:
        A.mesh.FlipNormals()

        
def smooth(A, iterations, relaxation_factor,
               feature_edge_smoothing=False, boundary_smoothing=True,
               feature_angle=30, edge_angle=15, inplace=False):
    
    smooth = vtk.vtkSmoothPolyDataFilter()
    smooth.SetInputData(A.mesh)

    smooth.SetNumberOfIterations(iterations)
    smooth.SetRelaxationFactor(relaxation_factor)
    smooth.SetFeatureAngle(feature_angle)
    smooth.SetEdgeAngle(edge_angle)
    smooth.SetFeatureEdgeSmoothing(feature_edge_smoothing)
    smooth.SetBoundarySmoothing(boundary_smoothing)
    smooth.Update()

    A.mesh.Overwrite(smooth.GetOutput())
    A.mesh = A.mesh.GenerateNormals(inplace=False)


################################################################
    
def calcCurvs(A):
    npoints = A.mesh.points.shape[0]
    neighs = np.array([ap.get_connected_vertices(A.mesh, ii)
                       for ii in xrange(npoints)])
    curvs = A.mesh.Curvature('mean')
    
    return curvs, neighs


def smoothCurvs(A, curvs, neighs):
    curvs = boa.set_boundary_curv(curvs, A.mesh, np.min(curvs))
    curvs = boa.filter_curvature(curvs, neighs, np.min, 1)
    curvs = boa.filter_curvature(curvs, neighs, np.mean, 10)

    return curvs

def segmentCurv(A, curvs, neighs):
    pdata = boa.init_pointdata(A, curvs, neighs)

    #''' Identify BoAs'''
    pdata = boa.domains_from_curvature(pdata)
    boas, boasData = boa.get_boas(pdata)

    print boa.nboas(pdata)
    
    return pdata


def mergeDomains(A, pdata):
    pdata = boa.merge_boas_depth(A, pdata, threshold=0.003)
    boas, boasData = boa.get_boas(pdata)
    print boa.nboas(pdata)

    pdata = boa.merge_boas_distance(pdata, boas, boasData, 15)
    boas, boasData = boa.get_boas(pdata)
    print boa.nboas(pdata)

    pdata = boa.merge_boas_engulfing(A, pdata, threshold=0.6)
    boas, boasData = boa.get_boas(pdata)
    print boa.nboas(pdata)

    return pdata

        
def plotDomains(A, pdata):
    boas, boasData = boa.get_boas(pdata)
    boacoords = np.array([tuple(ii) for ii in boasData[['z', 'y', 'x']].values])

    pl.PlotPointData(A.mesh, pdata, 'domain',
                     boacoords=boacoords, show_boundaries=True)

    return

def saveDomain(A, pdata, i, fout):
    dom = boa.get_domain(A.mesh, pdata=pdata, domain=i)
    dom.Save('flower_prim.ply', mode='ascii')
    A.mesh.Write(fout,'ply', binary=False)
    
