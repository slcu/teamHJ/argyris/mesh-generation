from collections import defaultdict

def parseComment(f):
    f.readline()
    return

def parseHeader(f):
    f.readline()
    return

def parseLine(ln):
    def intToBool(i):
        if i == 2: return False
        elif i==3: return True
        else: raise ValueError("Gene status should be either 2 (off) or 3 (on)")
    
    (tp, rest) = ln.split(',')
    (cid, status) = rest.split(':')

    return (int(tp), int(cid), intToBool(int(status)))

def dstatus(d):
    return d[2]

def dtp(d):
    return d[0]

def dcid(d):
    return d[1]

def parseGeneFile(fn):
    geneSt = defaultdict(list)
    
    tps = set()
    f = open(fn, 'r')
    
    parseComment(f)
    parseHeader(f)

    data = [parseLine(ln) for ln in f]

    for d in data:
        if dstatus(d):
            geneSt[dtp(d)].append(dcid(d))

    
    return geneSt    
        
    #return a map from times to set of cell ids
    
