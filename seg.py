import numpy as np
import ast
from collections import defaultdict

geneNms = [
    'AP3',
    'REV',
    'SUP',
    'AHP6',
    'PHB_PHV',
    'SEP1_2',
    'PI',
    'LFY',
    'AS1',
    'AG',
    'ATML1',
    'STM',
    'AP1_2',
    'L2',
    'L1',
    'ETTIN',
    'SEP3',
    'FIL',
    'WUS',
    'PUCHI',
    'ANT',
    'CUC1_2_3',
    'MP',
    'CLV3',
    ]

class Vec3(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def dist(self, v1):
        return np.sqrt((self.x - v1.x)**2 +
                       (self.y - v1.y)**2 +
                       (self.z - v1.z)**2)

    def toOrganism(self):
        return ' '.join([str(self.x), str(self.y), str(self.z)])

    def __repr__(self):
        return self.toOrganism()

class Cell(object):
    def __init__(self, cid, pos, vol, exprs=None):
        self.cid = cid        #Int
        self.pos = pos        #Vec3
        self.vol = vol        #Real
        self.exprs = exprs    #Map String Bool

    def dist(self, c1):
        return self.pos.dist(c1.pos)

    def addExprs(self, exprs, geneNms):
        for gn in geneNms:
            if not gn in exprs.keys():
                exprs[gn] = False

        self.exprs = exprs
        self.geneNms = geneNms
        
        return

    def _boolToInt(self, b):
       if b: return 1
 
       return 0
    
    def _exprsToOrg(self):
        exprs = [str(self._boolToInt(self.exprs[gn])) for gn in self.geneNms]
        
        return ' '.join(exprs)

    @classmethod
    def fromTV(cls, nelems):
        def parsePos(spos):
            pos = list(np.fromstring(spos.replace('[', '').replace(']', ''),
                                     sep=' '))
            return pos
        
        cid = int(nelems['cid'])
        vol = float(nelems['volume'])
        [xc, yc, zc] = parsePos(nelems['center'])
        pos = Vec3(xc, yc, zc)

        return cls(cid, pos, vol)

    @classmethod
    def fromImage(cls, cd, res):
        volVoxel = np.prod(res)
        xc, yc, zc = cd.centroid
        nVoxels = cd.area

        xc, yc, zc = np.multiply(np.array([xc, yc, zc]), res)
        
        vol = nVoxels * volVoxel
        pos = Vec3(xc, yc, zc)
        cid = cd.label

        return cls(cid, pos, vol)

    def toOrganism(self):
        if self.exprs:
            return (' '.join([self.pos.toOrganism(), str(self.vol),
                          str(self.cid), self._exprsToOrg()]))
        else:
            return (' '.join([self.pos.toOrganism(), str(self.vol),
                              str(self.cid)]))

    def toText(self):
        if self.exprs:
            return (' '.join([str(self.cid), self.pos.toOrganism() ,
                              self._exprsToOrg()]))
        else:
            return (' '.join([str(self.cid), self.pos.toOrganism()]))

    def __repr__(self):
        return self.toOrganism()

class Tissue(object):
    def __init__(self, cells, neighs):
        self.cells = cells            #Map Int Cell
        self.neighs = neighs          #Map Int (Map Int Dist)

    def __iter__(self):
        #iterate over a list instead of map
        return iter([v for _, v  in self.cells.iteritems()])
        
    def _extractCells(self, data, res):
        from skimage.measure import regionprops
        cellsSeg = regionprops(data)

        cells = dict()
        for cd in cellsSeg:
            cells[int(cd.label)] = Cell.fromImage(cd, res)

        return cells

    def _getConn(self, data):
        from skimage.future.graph import RAG

        connG = RAG(data)
        neigh = dict()
        
        for n in connG.nodes:
            nbrs = connG.adj[n].keys()
            ds = [self.cells[n].dist(self.cells[nb]) for nb in nbrs]
            
            neigh[n] = dict(zip(nbrs, ds))

        return neigh

    @classmethod
    def fromTV(cls, fnGeom, fnNeigh):
        def parseLn(ln):
            ln = str.strip(ln)
            nelems = dict()
            elems = str.split(ln, ',')
            for elm in elems:
                [name, val] = str.split(elm, ':')
                name = str.strip(name)
                val = str.strip(val)
                nelems[name] = val
                
            return nelems
        
        cells = dict() 
        neighs = dict()
        
        with open(fnGeom, 'r') as f:
            for ln in f:
                nelems = parseLn(ln)
                c = Cell.fromTV(nelems)
                cells[c.cid] = c

        with open(fnNeigh, 'r') as f:
            for ln in f:
                nelems = ast.literal_eval(ln)
                cid = nelems['cid']
                ns = nelems['neighbors ids']
                try:
                    neighs[cid] = dict((n, cells[cid].dist(cells[n])) for n in ns)
                except KeyError:
                    pass

        return cls(cells, neighs)
    
    @classmethod
    def fromImage(cls, data, res):
        self.cells = self._extractCells(data, res)
        self.conn = self._getConn(data)

        return cls(cells, conn)

    def toNeigh(self):
        
        def cellToNeigh(cid, nbs):
            nbsIds = nbs.keys()
            nNbs = len(nbsIds)
            nbsDists = [nbs[nid] for nid in nbsIds]
            
            return " ".join( [str(cid), str(nNbs)]
                             + map(str, nbsIds)
                             + map(str, nbsDists))

        ncells = sum(1 for _ in self)
        header = str(ncells) + " " + "1"
        
        cellReprs = ([cellToNeigh(cid, nbs)
                      for cid, nbs in self.neighs.iteritems()])

        return "\n".join([header] + cellReprs)
        
    def toOrganism(self):
        headerComment = "#x y z vol id"
        ns = str(len(self.cells)) + " 5"
        content = '\n'.join([cell.toOrganism() for cell in self])

        return "\n".join([headerComment, ns, content])

    def toText(self):
        return "\n".join([cell.toText() for cell in self])

    
class STissue(Tissue):
    def __init__(self, cells, neighs, gexprs, geneNms):
        super(STissue, self).__init__(cells, neighs)
        self.addExprs(gexprs, geneNms)

    def _defGeneExprs(self):
        return dict([(gn, False) for gn in self.geneNms])
    
    def addExprs(self, gexprs, geneNms):
        self.geneNms = geneNms
        for cell in self:
            try:
                cell.addExprs(gexprs[cell.cid], geneNms)
            except KeyError:
                cell.addExprs(self._defGeneExprs(), geneNms)
                
        return

    @classmethod
    def fromImage(cls, data, res, gexprs, geneNms):
        ts = Tissue.fromImage(data, res)
        return cls(ts.cells, ts.neighs, gexprs, geneNms)

    @classmethod
    def fromTV(cls, fnGeom, fnNeigh, gexprs, geneNms):
        ts = Tissue.fromTV(fnGeom, fnNeigh)
        return cls(ts.cells, ts.neighs, gexprs, geneNms)

    def toOrganism(self):
        headerComment = "#x y z vol id " + ' '.join(self.geneNms)
        ns = str(len(self.cells)) + " " + str(len(self.geneNms)+5)
        content = '\n'.join([cell.toOrganism() for cell in self])

        return "\n".join([headerComment, ns, content])


###############
def readImage(fim, fseg):
    import phenotastic.file_processing as fp
    f = fp.tiffload(fseg)
    seg = f.data.astype(int)

    fg = fp.tiffload(fim)
    res = fp.get_resolution(fg)

    return (seg, res)

def readExprs(fn):
    gexprs = dict() #Map Int (Map String Bool)  Cid -> (GeneName -> {On/Off})
    
    with open(fn) as f:
        geneNms = f.readline().rstrip().split(' ')[1:]

        for line in f:
            els = line.rstrip().split(' ')
            cid = els[0]
            exprs = els[1:]
            gexprs[int(cid)] = dict(zip(geneNms, [bool(int(exp))
                                                  for exp in exprs]))
        
    return gexprs

def writeOrg(fnGeom, fnNeigh, ts):
    with open(fnGeom, 'w+') as f:
        f.write(ts.toOrganism())

    with open(fnNeigh, 'w+') as f:
        f.write(ts.toNeigh())

    return

def mkOrgs(d, dExprs, dOut):
    from os.path import join
    
    timepoints = [10, 40, 96, 120, 132]
    geomF = "_segmented_tvformat_0_volume_position.txt"
    neighF = "_segmented_tvformat_0_neighbors_py.txt"
    exprF = "h.txt"

    initF = ".init"
    orgNeighF = ".neigh"

    for t in timepoints:
        print t
        fnGeom = join(d, "t" + str(t) + geomF)
        fnNeigh = join(d, "t" + str(t) + neighF)
        fnExpr = join(dExprs, "t_" + str(t) + exprF)
        fnGeomOut = join(dOut, "t" + str(t) + initF)
        fnNeighOut = join(dOut, "t" + str(t) + orgNeighF)

        gexprs = readExprs(fnExpr)
        ts = STissue.fromTV(fnGeom, fnNeigh, gexprs, geneNms)

        writeOrg(fnGeomOut, fnNeighOut, ts)

    return

#functions for common tasks
def mkOrgGeomState(fim, fseg, fexpr, fout):
    (data, res) = readImage(fim, fseg)
    gexprs = readExprs(fexpr)

    ts = STissue.fromImage(data, res, gexprs, geneNms)

    writeOrg(fout, ts)

def mkOrgGeom(fim, fseg, fout):
    (data, res) = readImage(fim, fseg)
    ts = Tissue.fromImage(data, res)

    writeOrg(fout, ts)

#for command line running
def getParser():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('inputFile',
                        action="store",
                        type=argparse.FileType('r'),
                        help='File name for the image file to be converted')
    parser.add_argument('-e', '--expressionFile',
                        action="store",
                        type=argparse.FileType('r'),
                        default=False,
                        help="File to extract expression data")
    parser.add_argument('-o', '--outputFile',
                        action="store",
                        type=argparse.FileType('w'),
                        default=False,
                        help='File to store output in')

    return parser

def commandLineRunner():
    import os.path
    parser = getParser()
    args = parser.parse_args()
    
    fim = args.inputFile.name
    (fb, _) = os.path.splitext(fim)
    fseg = fb + "_segmented.tif"
    
    fexpr = args.expressionFile.name
    fout = args.outputFile.name

    if fexpr: mkOrgGeomState(fim, fseg, fexpr, fout)
    else: mkOrgGeom(fim, fseg, fout)

if __name__ == "__main__":
    commandLineRunner()
